import {ProductDetailData} from "./ProductData";

export interface TransactionData {
    tid:       number;
    buyer_uid: number;
    datetime:  string;
    status:    string;
    total:     number;
    items:     TransactionProductData[];
}

export interface TransactionProductData {
    tpid:     number;
    product:  ProductDetailData;
    quantity: number;
    subtotal: number;
}