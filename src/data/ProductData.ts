export interface ProductListingData {
    pid: number;
    name: string;
    price: number;
    image_url: string;
    has_stock: boolean;
}

export interface ProductDetailData {
    pid: number;
    name: string;
    description: string;
    price: number;
    stock: number;
    image_url: string;
}