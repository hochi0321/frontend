import {Button, Col, Container, Form, Row, Spinner, Table} from "react-bootstrap";
import NavbarTop from "../../component/NavbarTop";
import './style.css'
import React, {useEffect, useState} from "react";
import {TransactionData} from '../../../data/TransactionData'
import {Link, useNavigate, useParams} from "react-router-dom";
import {UserData} from "../../../data/UserData";
import {firebaseAuthServiceOnAuthStateChanged} from "../../../service/AuthService";
import {
    finishTransaction,
    getTransaction,
    updateTransaction
} from "../../../resource/TransactionResource";

type Params = {
    tid: string;
}

export default function Transaction() {
    const [currentUser, setCurrentUser] = useState<UserData | undefined | null>(undefined);
    const [transactionData, setTransactionData] = useState<TransactionData | undefined | null>(undefined);
    const [isButtonDisabled, setIsButtonDisabled] = useState<boolean>(false);

    const params = useParams<Params>();

    useEffect(() => {
        firebaseAuthServiceOnAuthStateChanged(onLoadedCurrentUser)
            getTransaction(params.tid!, onApiGetTransaction)
    }, [])

    let navigate = useNavigate();

    const onApiGetTransaction = (isSuccess: boolean, transactionData?: TransactionData) => {
        if (isSuccess) {
            setTransactionData(transactionData);
        }else {
            navigate(`/404`, {replace: true})
        }
    }

    const onLoadedCurrentUser = (currentUser: UserData | null) => {
        setCurrentUser(currentUser)
    }


    const pay = () => {
        if (transactionData) {
            updateTransaction(params.tid!, onApiUpdateTransaction)
            setIsButtonDisabled(true)
        }
    }


    const onApiUpdateTransaction = (isSuccess: boolean, transactionData?: TransactionData) => {
        if (isSuccess) {
            finishTransaction(params.tid!, onApiFinishTransaction)
            setTransactionData(transactionData)
        } else {
            navigate(`/404`, {replace: true})
        }
    }

    const onApiFinishTransaction = (isSuccess: boolean, transactionData?: TransactionData) => {
        if (isSuccess) {
            setTransactionData(transactionData);
            navigate(`/transaction/${params.tid}/thankyou`, {replace: true})
        } else {
            navigate(`/404`, {replace: true})
        }
    }
    return (
        <>
            <NavbarTop/>
            <Container>
                <h1 className='header'>Confirm Payment</h1>
                {(transactionData === undefined) ?
                    <Spinner animation={"border"} style={{position: "fixed", top: "48vh", left: "48vw"}}/>
                    :
                    (transactionData) ?
                        <>
                            <div className="transaction-split">
                                <div className="transaction-split-element">
                                    <Table striped hover style={{maxWidth: "100vw", objectFit: "scale-down"}}>
                                        <thead>
                                        <tr>
                                            <th className='img-col' style={{textAlign: 'center'}}/>
                                            <th>Product Name</th>
                                            <th style={{textAlign: 'center'}}>Quantity</th>
                                            <th style={{textAlign: 'center'}}>Subtotal</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {transactionData.items.map((value) => {
                                            return (
                                                <tr key={value.tpid}>
                                                    <td className='img-col' style={{textAlign: 'center'}}><Link
                                                        to={`/product/${value.product.pid}`}
                                                        className='cart-product-link'><img
                                                        src={value.product.image_url}
                                                        className='cart-product-image'/></Link>
                                                    </td>
                                                    <td><Link to={`/product/${value.product.pid}`}
                                                              className='cart-product-link'>{value.product.name}<br/></Link>${value.product.price}
                                                    </td>
                                                    <td style={{textAlign: 'center'}}>
                                                        {value.quantity}
                                                    </td>
                                                    <td style={{textAlign: 'center'}}>${value.subtotal}</td>
                                                </tr>
                                            )
                                        })}


                                        </tbody>
                                    </Table>
                                    <div className='total-container'>
                                        <div>
                                            <h4>Total: ${transactionData.total}</h4>
                                        </div>
                                    </div>
                                </div>
                                <div className="transaction-split-element">
                                    <Form>
                                        <h5>Pay with card</h5>


                                        <Form.Group className="mb-3">
                                            <Form.Label>Card number</Form.Label>
                                            <Form.Control placeholder="1234 1234 1234 1234"/>
                                        </Form.Group>
                                        <Row className="mb-3">
                                            <Form.Group as={Col} controlId="formGridCity">

                                                <Form.Label>Expiration Date</Form.Label>
                                                <Form.Control placeholder="MM/YY"/>
                                            </Form.Group>

                                            <Form.Group as={Col} controlId="formGridZip">
                                                <Form.Label>Security code</Form.Label>
                                                <Form.Control placeholder="CVV"/>
                                            </Form.Group>
                                        </Row>
                                        <Button variant='outline-dark' size={"lg"}
                                                style={{width: "100%", marginTop: "2rem", marginBottom: "2rem"}}
                                                onClick={pay}
                                        disabled={isButtonDisabled}>
                                            Pay ${transactionData.total}
                                        </Button>
                                    </Form>
                                </div>
                            </div>
                        </>
                        : <>
                            No transaction found.
                        </>
                }
            </Container>
        </>
    )
}