import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import './style.css'
import {FloatingLabel, Spinner} from "react-bootstrap";
import Placeholder from 'react-bootstrap/Placeholder';
import React, {useEffect, useState} from 'react';
import {ProductListingData} from "../../../data/ProductData";
import getProductListData from "../../../resource/ProductResource";
import {Link} from "react-router-dom";
import Navbar from "../../component/NavbarTop";
import Carousel from "../../component/Carousel";
import ProductResource from "../../../resource/ProductResource";
import {Form} from 'react-bootstrap';

export default function ProductListing() {
    const [productListingData, setProductListingData] = useState<ProductListingData[] | undefined>(undefined);

    const onLoadProductListingData = (status: number, data?: ProductListingData[]) => {
        if (status === 200) {
            setProductListingData(data);
        } else {
            console.log(status);
        }
    }

    const sortData = (productListingData: ProductListingData[], sortType: string) => {
        if (sortType === "1") {
            return productListingData.sort((a, b) => a.price - b.price);
        } else if (sortType === "2") {
            return productListingData.sort((a, b) => b.price - a.price);
        } else if (sortType === "3") {
            return productListingData.sort((a, b) => a.name.localeCompare(b.name));
        } else if (sortType === "4") {
            return productListingData.sort((a, b) => b.name.localeCompare(a.name));
        }
        return productListingData;
    }


    useEffect(() => {
        getProductListData(onLoadProductListingData);
    }, [])

    const [sortType, setSortType] = useState<string>('0');

    return (
        <>
            <Navbar/>
            <Carousel/>
            <div id='sorting-container'>
                <FloatingLabel controlId="floatingSelect" label="Sort By">
                <Form.Select aria-label="Default select example" id='sorting-form'
                             onChange={(e) => setSortType(e.target.value)}>
                    <option value="1">Price Lowest</option>
                    <option value="2">Price Highest</option>
                    <option value="3">Name (A-Z)</option>
                    <option value="4">Name (Z-A)</option>
                </Form.Select>
                </FloatingLabel>
            </div>
            <div id="product-listing-container">
                {
                    (productListingData) ?
                        sortData(productListingData, sortType).map((value, index) => (
                            <Card className="product-listing-card" key={value.pid}>
                                <Link to={`/product/${value.pid}`} className="product-listing-card-button-link">
                                    <Card.Img className='product-listing-card-img' variant="top" src={value.image_url}/>
                                </Link>
                                <Card.Body className='product-listing-card-body'>
                                    <Link to={`/product/${value.pid}`} className="product-listing-card-button-link">
                                        <Card.Title>{value.name}</Card.Title>
                                    </Link>
                                    <Card.Text>
                                        Price: ${value.price}
                                    </Card.Text>
                                    <Link to={`/product/${value.pid}`} className="product-listing-card-button-link">
                                        <Button className="product-listing-card-button" variant="outline-dark">
                                            Details
                                        </Button>
                                    </Link>
                                </Card.Body>
                            </Card>
                        ))
                        :
                        Array.from({length: 12}).map((value, index) => (
                            <Card className="product-listing-card" key={index}>
                                <div className="product-listing-card-img-spinner-container">
                                    <Spinner animation="border" variant="light"/>
                                </div>
                                <Card.Body>
                                    <Placeholder as={Card.Title} animation="glow">
                                        <Placeholder xs={12}/>
                                        <Placeholder xs={12}/>
                                        <Placeholder xs={10}/>
                                    </Placeholder>
                                    <Placeholder as={Card.Text} animation="glow">
                                        <Placeholder xs={3}/> <Placeholder xs={4}/>
                                    </Placeholder>
                                    <Placeholder.Button variant="outline-dark" xs={12}/>
                                </Card.Body>
                            </Card>
                        ))
                }
            </div>
        </>
    )
}