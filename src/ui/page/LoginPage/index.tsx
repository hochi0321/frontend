import React, {ChangeEvent, useState} from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import NavbarTop from "../../component/NavbarTop";
import './style.css'
import {Alert, Card, FloatingLabel} from "react-bootstrap";
import {
    firebaseAuthServiceSignInWithEmailAndPassword,
    firebaseAuthServiceSignInWithGoogle
} from "../../../service/AuthService";
import {useNavigate} from "react-router-dom";
import {GoogleLoginButton} from "react-social-login-buttons";

export default function Login() {
    const [email, setEmail] = useState<string>("");
    const [password, setPassword] = useState<string>("");
    const [isLoginInvalid, setIsLoginInvalid] = useState(false);
    let navigate = useNavigate();

    function validateForm() {
        return email.length > 0 && password.length > 0;
    }

    const handleEmailChange = (e: ChangeEvent<HTMLInputElement>) => setEmail(e.target.value);
    const handlePasswordChange = (e: ChangeEvent<HTMLInputElement>) => setPassword(e.target.value);
    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        firebaseAuthServiceSignInWithEmailAndPassword(email, password, onLoadedSignIn)
    }

    const onLoadedSignIn = (isSuccess: boolean) => {
        if (isSuccess) {
            navigate(-1)
        } else {
            setIsLoginInvalid(true);
        }
    }

    return (
        <>
            <NavbarTop/>
            <div className="login-container">
                <Card className='login-card-body'>
                    <Form className='login-form-body' onSubmit={handleSubmit}>
                        <h3>Login</h3>
                        <Form.Group controlId="email" className='login-form-group'>
                            <FloatingLabel
                                controlId="floatingInput"
                                label="Email address"
                                className="mb-3">
                                <Form.Control autoFocus
                                              type="email"
                                              placeholder="Enter your email address"
                                              value={email}
                                              onChange={handleEmailChange}/>
                            </FloatingLabel>
                            <FloatingLabel controlId="floatingPassword" label="Password">
                                <Form.Control type="password"
                                              placeholder="Enter your password"
                                              value={password}
                                              onChange={handlePasswordChange}/>
                            </FloatingLabel>
                        </Form.Group>
                        {(isLoginInvalid) &&
                            <Alert variant="danger" className="invalid-alert">
                                Invalid username or password
                            </Alert>
                        }
                        <Button className='login-button' variant='outline-dark' size="lg" type="submit"
                                disabled={!validateForm()}>
                            Login
                        </Button>
                        <div className='login-button'>
                            <GoogleLoginButton onClick={() => {
                                firebaseAuthServiceSignInWithGoogle(onLoadedSignIn)
                            }} />
                        </div>

                    </Form>

                </Card>
            </div>
        </>
    );
}