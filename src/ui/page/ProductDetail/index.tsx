import React, {useEffect, useState} from "react";
import {ProductDetailData} from "../../../data/ProductData";
import Card from "react-bootstrap/Card";
import './style.css'
import { Button, Container, Spinner, Table, Toast} from "react-bootstrap";
import Selector from "../../component/Selector";
import Navbar from "../../component/NavbarTop";
import {useParams, Navigate, useNavigate} from "react-router-dom";
import {getProductDetailData} from "../../../resource/ProductResource";
import {putCartItem} from "../../../resource/CartResource";
import {firebaseAuthServiceGetAccessToken} from "../../../service/AuthService";

type Params = {
    pid: string;
}

export default function ProductDetail() {
    const [productDetailData, setProductDetailData] = useState<ProductDetailData | undefined | null>(undefined);
    const [show, setShow] = useState<boolean>(false);

    const OnLoadProductDetailData = (data: ProductDetailData | null) => {
        setProductDetailData(data)
    }

    const params = useParams<Params>();

    let navigate = useNavigate();

    useEffect(() => {
            if (params.pid) {
                getProductDetailData(parseInt(params.pid), OnLoadProductDetailData)
            } else {
                navigate("/404", {replace: true})
            }
        }
        , [])

    const [quantity, setQuantity] = useState<number>(1);

    let setQuantityMinusOne = () => {
        if (quantity > 1) {
            setQuantity(quantity - 1);
        }
    }
    let setQuantityPlusOne = () => {
        if (productDetailData!.stock > quantity) {
            setQuantity(quantity + 1);
        }
    }

    let addToCart = () => {
        if (firebaseAuthServiceGetAccessToken() === null) {
            navigate('/login', {replace: true})
        }
        if (productDetailData) {
            putCartItem(productDetailData.pid, quantity, onApiPutCartItem)
        }
    }

    const onApiPutCartItem = (isSuccess: boolean) => {
        if (isSuccess) {
            setShow(true);
        }
    }

    return (
        <>
            <Navbar/>
                <Toast onClose={() => setShow(false)} show={show} delay={3000} autohide
                       style={{position: 'fixed', zIndex: 1, right: "0"}}>
                    <Toast.Body>Added to your Shopping Cart.</Toast.Body>
                </Toast>
            <Container className="product-detail-container">
                {productDetailData ?
                    <Card className="product-detail-card-body">
                        <div className="product-detail-split">
                            <div className="product-detail-card-img-container">
                                <img className="product-detail-img" src={productDetailData.image_url}/>
                            </div>
                            <div className="product-detail-description">
                                <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                                    <h4>{productDetailData.name}</h4>
                                    <p style={{whiteSpace: "pre-line",}}>
                                        {productDetailData.description}
                                    </p>

                                    <Table borderless={true} style={{maxWidth: '5rem'}}>
                                        <tbody style={{textAlign: 'center'}}>
                                        <tr>
                                            <td>Price:</td>
                                            <td>${productDetailData.price}</td>
                                        </tr>
                                        <tr>
                                            <td>Stock:</td>
                                            <td>{productDetailData.stock}</td>
                                        </tr>
                                        <tr>
                                            <td>Quantity:</td>
                                            <td style={{textAlign: 'center'}}><Selector
                                                quantity={quantity}
                                                setQuantityMinusOne={setQuantityMinusOne}
                                                setQuantityPlusOne={setQuantityPlusOne}
                                            /></td>
                                        </tr>
                                        </tbody>
                                    </Table>
                                </div>
                                <div className="d-grid gap-2">
                                    {(productDetailData?.stock > 0) ?
                                        <Button variant="outline-dark" size="lg" onClick={addToCart}>
                                            Add to Cart
                                        </Button>
                                        : <Button variant="outline-danger" size="lg" disabled={true}>
                                            Out of Stock
                                        </Button>
                                    }
                                </div>
                            </div>
                        </div>
                    </Card>
                    : (productDetailData === undefined) ?
                        <Spinner animation={"border"} style={{position: "fixed", top: "48vh", left: "48vw"}}/>
                        : <Navigate to="/404" replace/>
                }
            </Container>
        </>
    )
}