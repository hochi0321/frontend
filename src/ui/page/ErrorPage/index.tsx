import {Container} from "react-bootstrap";
import NavbarTop from "../../component/NavbarTop";

export default function ErrorPage() {
    return (
        <>
            <NavbarTop/>
            <Container>404</Container>
        </>
    )
}