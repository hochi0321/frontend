import NavbarTop from "../../component/NavbarTop";
import React, {useEffect, useState} from "react";
import {Container, Spinner, Table} from "react-bootstrap";
import Tick from './checked.png';
import './style.css'
import {UserData} from "../../../data/UserData";
import {TransactionData} from "../../../data/TransactionData";
import {Link, useNavigate, useParams} from "react-router-dom";
import {firebaseAuthServiceOnAuthStateChanged} from "../../../service/AuthService";
import {getTransaction} from "../../../resource/TransactionResource";

type Params = {
    tid: string;
}

export default function ThankYou() {
    const [currentUser, setCurrentUser] = useState<UserData | undefined | null>(undefined);
    const [transactionData, setTransactionData] = useState<TransactionData | undefined | null>(undefined);

    const params = useParams<Params>();

    useEffect(() => {
        firebaseAuthServiceOnAuthStateChanged(onLoadedCurrentUser)
        getTransaction(params.tid!, onApiGetTransaction)
    }, [])

    let navigate = useNavigate();

    const onApiGetTransaction = (isSuccess: boolean, transactionData?: TransactionData) => {
        if (isSuccess) {
            setTransactionData(transactionData);
        } else (
            navigate('/404', {replace: true})
        )
    }

    const onLoadedCurrentUser = (currentUser: UserData | null) => {
        setCurrentUser(currentUser)
    }


    return (
        <>
            <NavbarTop/>
            <Container>
                {(transactionData === undefined) ?
                    <Spinner animation={"border"} style={{position: "fixed", top: "48vh", left: "48vw"}}/>
                    :
                    (transactionData) ?
                        <>
                            <div className="transaction-split-element">
                                <div className='tick-img-container'>
                                    <img src={Tick} id='tick-img'/>
                                </div>
                                <div className='text-container'>
                                    <h2>Thank You!</h2>
                                    <h3>Transaction Completed!</h3>
                                </div>
                            </div>
                            <div className="transaction-split-element">
                                <h3>Transaction Details</h3>
                                <Table striped hover style={{maxWidth: "100vw", objectFit: "scale-down"}}>
                                    <thead>
                                    <tr>
                                        <th className='img-col' style={{textAlign: 'center'}}/>
                                        <th>Product Name</th>
                                        <th style={{textAlign: 'center'}}>Quantity</th>
                                        <th style={{textAlign: 'center'}}>Subtotal</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {transactionData.items.map((value) => {
                                        return (
                                            <tr key={value.tpid}>
                                                <td className='img-col' style={{textAlign: 'center'}}><Link
                                                    to={`/product/${value.product.pid}`}
                                                    className='cart-product-link'><img
                                                    src={value.product.image_url}
                                                    className='cart-product-image'/></Link>
                                                </td>
                                                <td><Link to={`/product/${value.product.pid}`}
                                                          className='cart-product-link'>{value.product.name}<br/></Link>${value.product.price}
                                                </td>
                                                <td style={{textAlign: 'center'}}>
                                                    {value.quantity}
                                                </td>
                                                <td style={{textAlign: 'center'}}>${value.subtotal}</td>
                                            </tr>
                                        )
                                    })}


                                    </tbody>
                                </Table>
                                <div className='total-container'>
                                    <div>
                                        <h4>Total: ${transactionData.total}</h4>
                                    </div>
                                </div>
                            </div>

                        </>
                        : <>
                            No transaction found.
                        </>
                }
            </Container>
        </>
    )
}