import {Button, Container, Spinner, Table} from "react-bootstrap";
import NavbarTop from "../../component/NavbarTop";
import './style.css'
import React, {useEffect, useState} from "react";
import {CartItemData} from '../../../data/CartItemData'
import Selector from "../../component/Selector";
import {Link, useNavigate} from "react-router-dom";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {solid} from '@fortawesome/fontawesome-svg-core/import.macro'
import {UserData} from "../../../data/UserData";
import {firebaseAuthServiceGetAccessToken, firebaseAuthServiceOnAuthStateChanged} from "../../../service/AuthService";
import {getCartItemList, patchCartItem, removeCartItem} from "../../../resource/CartResource";
import {postTransaction} from "../../../resource/TransactionResource";
import {TransactionData} from "../../../data/TransactionData"; // <-- import styles to be used

export default function ShoppingCart() {
    const [currentUser, setCurrentUser] = useState<UserData | undefined | null>(undefined);
    const [cartItemDataList, setCartItemDataList] = useState<CartItemData[] | undefined | null>(undefined);

    let navigate = useNavigate();

    const calTotal = (): number => {
        let total: number = 0;
        cartItemDataList?.forEach((value) => {
            total += value.price * value.cart_quantity;
        })
        return total;
    }

    useEffect(() => {
        firebaseAuthServiceOnAuthStateChanged(onLoadedCurrentUser)
        if (firebaseAuthServiceGetAccessToken() === null) {
            navigate('/login', {replace: true})
        }
    }, [])

    const onLoadedCurrentUser = (currentUser: UserData | null) => {
        setCurrentUser(currentUser)
    }

    const onLoadedCartItemData = (cartItemDataList: CartItemData[] | null) => {
        setCartItemDataList(cartItemDataList)
    }

    useEffect(() => {
        getCartItemList(onLoadedCartItemData);
    }, [currentUser])

    const onApiPatchCartItem = (isSuccess: boolean, pid?: number, quantity?: number) => {
        if (isSuccess && cartItemDataList) {
            setCartItemDataList(
                cartItemDataList.map((value) => {
                    if (value.pid === pid) {
                        value.cart_quantity = quantity!;
                    }
                    return value;
                })
            )
        }
    }

    const setQuantityMinusOne = (pid: number, quantity: number) => {
        if (quantity > 1) {
            patchCartItem(pid, quantity - 1, onApiPatchCartItem);
        }
    }
    const setQuantityPlusOne = (pid: number, quantity: number, stock: number) => {
        if (quantity < stock) {
            patchCartItem(pid, quantity + 1, onApiPatchCartItem);
        }
    }

    const onApiRemoveCartItem = (isSuccess: boolean, pid?: number) => {
        if (isSuccess && cartItemDataList) {
            setCartItemDataList(cartItemDataList.filter((value) => {
                    return value.pid !== pid;
                }
            ))
        }
    }

    const checkOut = () => {
        postTransaction(onApiPostTransaction);
    }

    const onApiPostTransaction = (isSuccess: boolean, transactionData?: TransactionData) => {
        if (isSuccess) {
            navigate(`/transaction/${transactionData!.tid}`)
        }
    }

    return (
        <>
            <NavbarTop/>
            <Container className="cart-container">
                <h1 className='header'>Shopping Cart</h1>
                {(cartItemDataList === undefined) ?
                    <Spinner animation={"border"} style={{position: "fixed", top: "48vh", left: "48vw"}}/>
                    : (cartItemDataList === null || cartItemDataList.length === 0) ?
                        <h3>Your Shopping Cart is empty.</h3>
                        :
                        <div>
                            <Table striped hover style={{maxWidth: "100vw", objectFit: "scale-down"}}>
                                <thead>
                                <tr>
                                    <th className='img-col' style={{textAlign: 'center'}}/>
                                    <th>Product Name</th>
                                    <th style={{textAlign: 'center'}}>Quantity</th>
                                    <th style={{textAlign: 'center'}}>Subtotal</th>
                                    <th style={{textAlign: 'center'}}/>
                                </tr>
                                </thead>
                                    <tbody>
                                    {cartItemDataList.map((value) => {
                                        return (
                                            <tr key={value.pid}>
                                                <td className='img-col' style={{textAlign: 'center'}}><Link
                                                    to={`/product/${value.pid}`}
                                                    className='cart-product-link'><img
                                                    src={value.image_url} className='cart-product-image'/></Link></td>
                                                <td><Link to={`/product/${value.pid}`}
                                                          className='cart-product-link'>{value.name}<br/></Link>${value.price}
                                                </td>
                                                <td style={{textAlign: 'center'}}>
                                                    <Selector quantity={value.cart_quantity}
                                                              setQuantityPlusOne={() => {
                                                                  setQuantityPlusOne(value.pid, value.cart_quantity, value.stock)
                                                              }}
                                                              setQuantityMinusOne={() => {
                                                                  setQuantityMinusOne(value.pid, value.cart_quantity)
                                                              }}/>
                                                </td>
                                                <td style={{textAlign: 'center'}}>${value.cart_quantity * value.price}</td>
                                                <td style={{textAlign: 'center'}}>
                                                    <Button variant='outline-dark' onClick={() => {
                                                        removeCartItem(value.pid, onApiRemoveCartItem)
                                                    }}>
                                                        <FontAwesomeIcon icon={solid('trash-can')} size="lg"/>
                                                    </Button>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                    </tbody>
                            </Table>
                            <div className='total-container'>
                                    <h4>Total: ${calTotal()}</h4>
                            </div>
                            <Button variant='outline-dark' size={"lg"} style={{width:"100%", marginBottom:"2rem"}} onClick={checkOut}>Check
                                Out</Button>
                        </div>
                }
            </Container>
        </>
    )
}