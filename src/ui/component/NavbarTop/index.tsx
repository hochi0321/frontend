import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import {Button, Form, Spinner} from "react-bootstrap";
import './style.css';
import React, {useEffect, useState} from "react";
import Offcanvas from "react-bootstrap/Offcanvas";
import {Link, useNavigate} from "react-router-dom";
import logo from './logo.png'
import {UserData} from "../../../data/UserData";
import {firebaseAuthServiceOnAuthStateChanged, firebaseAuthServiceSignOut} from "../../../service/AuthService";
import {solid} from "@fortawesome/fontawesome-svg-core/import.macro";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


export default function NavbarTop() {
    const [currentUser, setCurrentUser] = useState<UserData | undefined | null>(undefined);
    const [show, setShow] = useState<boolean>(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    useEffect(() => {
        firebaseAuthServiceOnAuthStateChanged(onLoadedCurrentUser)
    }, [])

    const onLoadedCurrentUser = (currentUser: UserData | null) => {
        setCurrentUser(currentUser)
    }

    let navigate = useNavigate();

    const onLoginSuccess = () => {
        if (currentUser) {
            return (
                <Button variant="outline-light" onClick={() => {
                    firebaseAuthServiceSignOut()
                    navigate('/', {replace: true})
                }}>
                    Logout
                </Button>
            )
        } else if (currentUser === null) {
            return <Link to="/login">
                <Button variant="outline-light">Login</Button>
            </Link>
        } else if (currentUser === undefined) {
            return <Button variant="outline-light"><Spinner animation={"border"}
                                                            style={{width: '1rem', height: '1rem'}}/></Button>
        }
    }

    return (
        <>
            <Navbar bg="dark" variant="dark" sticky="top">
                <Container>
                    <Link to="/" className='nav-link'>
                        <Navbar.Brand>
                            <img
                                alt=""
                                src={logo}
                                width="30"
                                height="30"
                                className="d-inline-block align-top"
                            />{"    "}
                            Audio Lab
                        </Navbar.Brand>
                    </Link>
                    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                        </Nav>
                        <Form className="nav-form">
                            {onLoginSuccess()}
                            {(currentUser) &&
                                <Link to="/shoppingcart/" className='navbar-link'>
                                    <Button variant="outline-light">
                                        <FontAwesomeIcon icon={solid('cart-shopping')} size="lg"/>
                                    </Button>
                                </Link>
                            }
                        </Form>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            <Offcanvas show={show} onHide={handleClose} placement="end">
                <Offcanvas.Header closeButton>
                    <Offcanvas.Title>Shopping Cart</Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body>
                    {(currentUser) ?
                        <p>Hi, {currentUser.email}!</p>
                        :
                        <p>Please login</p>
                    }
                </Offcanvas.Body>
            </Offcanvas>

        </>
    );
}