import Carousel from 'react-bootstrap/Carousel';
import b1 from './banner1.png';
import b2 from './banner2.png';
import './style.css'

function CarouselFadeExample() {

    return (
        <>
                <Carousel fade>
                    <Carousel.Item>
                        <img
                            className="d-block w-100 img-small"
                            src={b1}
                            alt="First slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block w-100 img-full"
                            src={b2}
                            alt="Second slide"
                        />
                    </Carousel.Item>
                </Carousel>
        </>
    );
}

export default CarouselFadeExample;