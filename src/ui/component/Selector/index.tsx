import "./style.css";
import {Button} from "react-bootstrap";

type Props = {
    quantity: number,
    setQuantityMinusOne: () => void,
    setQuantityPlusOne: () => void
}

export default function Selector(props: Props) {
    return (
        <div>
            <div className="selector-container">
                <Button variant="outline-dark" size="sm" onClick={props.setQuantityMinusOne}>-</Button>
                <div className="selector-number">{props.quantity}</div>
                <Button variant="outline-dark" size="sm" onClick={props.setQuantityPlusOne}>+</Button>
            </div>
        </div>
    )
}