import {ProductDetailData, ProductListingData} from "../data/ProductData";
import axios, {AxiosResponse} from "axios";
import {CartItemData} from "../data/CartItemData";
import {firebaseAuthServiceGetAccessToken} from "../service/AuthService";
import getEnvConfig from "../config/Config";

export default function getProductListData(onLoadProductListingData: (status: number, productListingData: ProductListingData[]) => void) {
    axios.get(`${getEnvConfig().baseUrl}/public/product/`)
        .then((response: AxiosResponse<ProductListingData[]>) => {
                console.log(response.status, response.data);
                onLoadProductListingData(response.status, response.data);
            }
        ).catch((error) => {
        console.log(error.message);
    })
}

export function getProductDetailData(pid: number, onLoadProductDetailData: (productDetailData: ProductDetailData | null) => void) {
    axios.get(`${getEnvConfig().baseUrl}/public/product/${pid}`).then((response: AxiosResponse<ProductDetailData>) => {
        console.log(response.status, response.data);
        onLoadProductDetailData(response.data)
    }).catch((error) => {
        console.log(error.message);
        onLoadProductDetailData(null);
    })
}
export function getUserCartData(onLoadUserCartData: (cartItemData: CartItemData[]) => void) {
    firebaseAuthServiceGetAccessToken()
    axios.get(`${getEnvConfig().baseUrl}/cart`).then((response: AxiosResponse<CartItemData[]>) => {
        console.log(response.status, response.data);
        onLoadUserCartData(response.data)
    }).catch((error) => {
        console.log(error.message);
    })
}

