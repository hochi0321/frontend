import axios from "axios";
import {CartItemData} from "../data/CartItemData";
import {firebaseAuthServiceGetAccessToken} from "../service/AuthService";
import getEnvConfig from "../config/Config";

export function getCartItemList(setCartItemDataList: (cartItemDataList: CartItemData[] | null) => void) {
    firebaseAuthServiceGetAccessToken()
        ?.then((token: string) => {
            return axios.get<CartItemData[]>(`${getEnvConfig().baseUrl}/cart/`, {headers: {Authorization: "Bearer " + token}})
        })
        .then((response) => {
            setCartItemDataList(response.data);
        }).catch((error) => {
            console.log(error);
            setCartItemDataList(null);
        }
    )
}

export function removeCartItem(pid: number, onApiRemoveCartItem: (isSuccess: boolean, pid?: number) => void) {
    firebaseAuthServiceGetAccessToken()
        ?.then((token: string) => {
            return axios.delete(`${getEnvConfig().baseUrl}/cart/${pid}`, {headers: {Authorization: "Bearer " + token}})
        })
        .then(() => {
            onApiRemoveCartItem(true, pid);
        }).catch((error) => {
            console.log(error);
            onApiRemoveCartItem(false);
        }
    )
}

export function patchCartItem(pid: number, quantity: number, onApiPatchCartItem: (isSuccess: boolean, pid?: number, quantity?: number) => void) {
    firebaseAuthServiceGetAccessToken()
        ?.then((token: string) => {
            return axios.patch(`${getEnvConfig().baseUrl}/cart/${pid}/${quantity}`, undefined, {headers: {Authorization: "Bearer " + token}})
        })
        .then(() => {
            onApiPatchCartItem(true, pid, quantity);
        }).catch((error) => {
            console.log(error);
            onApiPatchCartItem(false);
        }
    )
}

export function putCartItem(pid: number, quantity: number, onApiPutCartItem: (isSuccess: boolean) => void) {
    firebaseAuthServiceGetAccessToken()
        ?.then((token: string) => {
            return axios.put(`${getEnvConfig().baseUrl}/cart/add-item/${pid}/${quantity}`, undefined, {headers: {Authorization: "Bearer " + token}})
        })
        .then(() => {
            onApiPutCartItem(true);
        }).catch(() => {
            onApiPutCartItem(false);
        }
    )
}

