import axios, {AxiosResponse} from "axios";
import {firebaseAuthServiceGetAccessToken} from "../service/AuthService";
import {TransactionData} from "../data/TransactionData";
import getEnvConfig from "../config/Config";

export function postTransaction(onApiPostTransaction: (isSuccess: boolean,transactionData?: TransactionData) => void) {
    firebaseAuthServiceGetAccessToken()
        ?.then((token: string) => {
            return axios.post(`${getEnvConfig().baseUrl}/transaction/prepare`, undefined, {headers: {Authorization: "Bearer " + token}})
        })
        .then((response:AxiosResponse<TransactionData>) => {
            onApiPostTransaction(true,response.data);
        }).catch(() => {
            onApiPostTransaction(false);
        }
    )
}

export function updateTransaction(tid:string,onApiUpdateTransaction: (isSuccess: boolean,transactionData?: TransactionData) => void) {
    firebaseAuthServiceGetAccessToken()
        ?.then((token: string) => {
            return axios.patch(`${getEnvConfig().baseUrl}/transaction/${tid}`, undefined, {headers: {Authorization: "Bearer " + token}})
        })
        .then((response:AxiosResponse<TransactionData>) => {
            onApiUpdateTransaction(true,response.data);
        }).catch(() => {
            onApiUpdateTransaction(false);
        }
    )
}
export function finishTransaction(tid:string,onApiFinishTransaction: (isSuccess: boolean,transactionData?: TransactionData) => void) {
    firebaseAuthServiceGetAccessToken()
        ?.then((token: string) => {
            return axios.patch(`${getEnvConfig().baseUrl}/transaction/${tid}/finish`, undefined, {headers: {Authorization: "Bearer " + token}})
        })
        .then((response:AxiosResponse<TransactionData>) => {
            onApiFinishTransaction(true,response.data);
        }).catch(() => {
            onApiFinishTransaction(false);
        }
    )
}
export function getTransaction(tid:string,onApiGetTransaction: (isSuccess: boolean,transactionData?: TransactionData) => void) {
    firebaseAuthServiceGetAccessToken()?.then((token: string) => {
            return axios.get(`${getEnvConfig().baseUrl}/transaction/${tid}`, {headers: {Authorization: "Bearer " + token}})
        })
        .then((response:AxiosResponse<TransactionData>) => {
            onApiGetTransaction(true,response.data);
        }).catch(() => {
            onApiGetTransaction(false);
        }
    )
}

