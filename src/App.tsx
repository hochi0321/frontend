import React, {useEffect, useState} from 'react';
import './App.css';
import ProductListing from "./ui/page/ProductListing";
import ProductDetail from "./ui/page/ProductDetail";
import {HashRouter, Route, Routes} from "react-router-dom";
import ScrollTop from './ui/component/ScrollTop';
import ErrorPage from "./ui/page/ErrorPage";
import LoginPage from "./ui/page/LoginPage";
import {firebaseAuthServiceOnAuthStateChanged} from "./service/AuthService";
import ShoppingCart from "./ui/page/ShoppingCart";
import Transaction from "./ui/page/Transaction";
import ThankYouPage from './ui/page/ThankYouPage';

function App() {
    const [isInitialized, setIsInitialized] = useState<boolean>(false);

    useEffect(() => {
        firebaseAuthServiceOnAuthStateChanged(() => {
            setIsInitialized(true);
        })
    },[])

    return (
        <>
            <div className="App">
                <HashRouter>
                    <ScrollTop/>
                    <Routes>
                        <Route path="/" element={<ProductListing/>}/>
                        <Route path="/product/:pid" element={<ProductDetail/>}/>
                        <Route path="/shoppingcart/" element={<ShoppingCart />}/>
                        <Route path="/login/" element={<LoginPage/>}/>
                        <Route path="/transaction/:tid" element={<Transaction/>}/>
                        <Route path="/transaction/:tid/thankyou" element={<ThankYouPage/>}/>
                        <Route path="/404" element={<ErrorPage/>}/>
                    </Routes>
                </HashRouter>
            </div>
        </>
    );
}

export default App;
